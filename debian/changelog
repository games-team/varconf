varconf (1.0.1-8) unstable; urgency=high

  * Force build with C++11 standard to fix FTBFS (Closes: #984384)
  * Update standards to 4.6.0 (no changes)
  * Update d/upstream/metadata

 -- Olek Wojnar <olek@debian.org>  Fri, 19 Nov 2021 10:53:33 -0500

varconf (1.0.1-7) unstable; urgency=medium

  * Remove d/symbols due to continued instability (Closes: #957906)
  * Update compatibility mode to 13 (remove d/compat)
  * Update standards to 4.5.0 (no changes)
  * Add Rules-Requires-Root: no

 -- Olek Wojnar <olek@debian.org>  Fri, 31 Jul 2020 20:37:02 -0400

varconf (1.0.1-6) unstable; urgency=high

  * Update d/control: new uploader email and URLs
  * Update d/copyright: refresh debian info
  * Update d/control: remove versioned build dependency on g++
  * Update symbols for GCC-8 (Closes: #897883)
  * Update d/control: standards 4.2.1 (no changes)
  * Update d/compat: 11 (no changes)
  * Remove d/triggers: redundant trigger
  * Add d/upstream/metadata: for DEP 12 compliance
  * Update d/control: migration complete, remove dbgsym-migration flag

 -- Olek Wojnar <olekw.dev@gmail.com>  Wed, 03 Oct 2018 18:36:39 -0400

varconf (1.0.1-5) unstable; urgency=medium

  * Build-depend on g++ >= 4:7 for GCC 7 compliance

 -- Olek Wojnar <olek-dev@wojnar.org>  Tue, 05 Sep 2017 13:10:35 -0500

varconf (1.0.1-4) unstable; urgency=high

  * Update dh compat to 10
  * Update symbols for GCC 7 (Closes: #853388)
  * Update standards to 4.1.0
    - d/control: document new standards version
    - d/copyright: use https in Format field
  * Update d/copyright: new copyright year for debian directory

 -- Olek Wojnar <olek-dev@wojnar.org>  Sat, 02 Sep 2017 22:01:28 -0500

varconf (1.0.1-3) unstable; urgency=high

  * Update symbols for GCC6 (Closes: #831212)

 -- Olek Wojnar <olek-dev@wojnar.org>  Thu, 14 Jul 2016 19:20:47 -0400

varconf (1.0.1-2) unstable; urgency=medium

  * Fix d/control Vcs-Git line

 -- Olek Wojnar <olek-dev@wojnar.org>  Fri, 24 Jun 2016 16:44:13 -0400

varconf (1.0.1-1) unstable; urgency=medium

  * New upstream release
  * Update d/control
    - Fix Vcs-Browser line (Thanks to Tobias Frost)
    - Remove pre-dependency on dpkg
    - Update standards to 3.9.8 (no changes)
  * Add myself as new uploader
    - Remove Stephen Webb per his request
    - Thanks for all the contributions, Stephen!
  * Update symbols (Closes: #805239)
  * Update d/watch: add file extensions potentially used by upstream
  * Update d/rules: remove builddeb override since xz is now default
  * Add d/triggers for proper ldconfig functionality
  * Enable all hardening options
  * Convert to automatic debug package

 -- Olek Wojnar <olek-dev@wojnar.org>  Sat, 11 Jun 2016 20:52:12 -0500

varconf (1.0.0-2) unstable; urgency=medium

  * Team upload.

  [ Stephen M. Webb ]
  * debian/copyright: update dates

  [ Simon McVittie ]
  * Rename libvarconf-1.0-8 to libvarconf-1.0-8v5 for g++-5 transition.
    Based on Ubuntu patches from Steve Langasek and Daniel T Chen.
    (Closes: #791308)
    - Update symbols file (Closes: #778160)
    - Build-depend on the new ABI of libsigc++-2.0 so everything happens
      in the right order

 -- Simon McVittie <smcv@debian.org>  Tue, 18 Aug 2015 21:01:52 +0100

varconf (1.0.0-1) unstable; urgency=low

  * new upstream release
  * bumped packaging name for new SONAME
  * debian/libvarconf-1.0-8.symbols: adjusted for symbol hiding
  * debian/control: updated Standards-Version to 3.9.4 (updated VCS-* fields)
  * debian/libvarconf-1.0-8.symbols: adjusted for GCC-4.8 (closes: #701369)

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Wed, 05 Jun 2013 11:11:32 -0400

varconf (0.6.7-2) unstable; urgency=low

  * optioned more symbols for GCC 4.7 builds (closes: #667408)
  * debian/control: updates Standards-Version to 3.9.3 (no changes)
  * incorporate and acknowledge NMU (thanks Matthias)

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Wed, 30 May 2012 09:28:48 -0400

varconf (0.6.7-1.1) unstable; urgency=low

  * Non maintainer upload
  * Fix build failure with GCC 4.7. Closes: #667408.

 -- Matthias Klose <doko@debian.org>  Wed, 30 May 2012 06:13:46 +0000

varconf (0.6.7-1) unstable; urgency=low

  * new upstream release
  * converted packaging to use dh sequenceer
  * new maintainer: Debian games team (closes: #653978)
    - added myself as uploader
  * converted to 3.0 (quilt) source format
  * changed package names due to SONAME version bump
  * updated Standards-Version to 3.9.2 (no changes required)
  * debian/control:  added Vcs- fields
  * added symbols tracking
  * debian/copyright: convert to DEP-5 format
  * debian/rules: regenerate autoconfigury
  * use xz compression for packaging
  * move to debhelper compat level 9

 -- Stephen M. Webb <stephen.webb@bregmasoft.ca>  Fri, 27 Jan 2012 21:38:19 -0500

varconf (0.6.6-2) unstable; urgency=low

  * debian/libvarconf-dev.install: Don't install *.la file.
  * debian/control: Updated Standards-Version to 3.8.3.

 -- Michael Koch <konqueror@gmx.de>  Tue, 25 Aug 2009 21:49:55 +0200

varconf (0.6.6-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Updated Build-Depends on debhelper to >= 5.
  * debian/control: Updated Standards-Version to 3.8.2.
  * debian/control: Move libvarconf-1.0-6-dbg to debug section.
  * debian/watch: Use SourceForge redirector.

 -- Michael Koch <konqueror@gmx.de>  Wed, 22 Jul 2009 16:51:55 +0200

varconf (0.6.5-4) unstable; urgency=low

  * Added patch to be able to build with GCC 4.3. Closes: #474867
  * Use chrpath to delete rpath.
  * Clarified debian/copyright.

 -- Michael Koch <konqueror@gmx.de>  Tue, 08 Apr 2008 10:33:45 +0200

varconf (0.6.5-3) unstable; urgency=low

  * Replaced ${Source-Version} in debian/control.
  * Updated clean target in debian/rules to give better error message on
    error.
  * Updated Standards-Version to 3.7.3.
  * Added Homepage field.

 -- Michael Koch <konqueror@gmx.de>  Mon, 24 Dec 2007 11:18:54 +0100

varconf (0.6.5-2) unstable; urgency=low

  * Upload to unstable.

 -- Michael Koch <konqueror@gmx.de>  Mon, 04 Jun 2007 21:50:10 +0200

varconf (0.6.5-1) experimental; urgency=low

  * Upload to experimental.
  * New upstream release.
  * Build-Depends on debhelper >= 4.0.0.
  * Fixed creation of debug package.
  * Updated Standards-Version to 3.7.2.

 -- Michael Koch <konqueror@gmx.de>  Tue, 13 Feb 2007 09:02:23 +0000

varconf (0.6.4-1) unstable; urgency=low

  * New upstream release

 -- Michael Koch <konqueror@gmx.de>  Mon, 23 Jan 2006 09:46:15 +0000

varconf (0.6.3-2) unstable; urgency=low

  * Use libsigc++-2.0 instead of libsigc++-1.2. This changes soname.
    (Closes: #339274).
  * debian/compat: Use compat level 4.

 -- Michael Koch <konqueror@gmx.de>  Sat,  3 Dec 2005 22:07:54 +0000

varconf (0.6.3-1) unstable; urgency=low

  * New upstream release
    - Updated to new SONAME
  * Updated watch file to use SourceForge
  * Tightened (Build-)Depends on libsigc++-1.2-dev to use (>= 1.2.5-5)

 -- Michael Koch <konqueror@gmx.de>  Wed, 12 Oct 2005 06:08:14 +0000

varconf (0.6.2-3) unstable; urgency=low

  * Fixed typo in package description of libvarconf-1.0-2
    (Closes: #325660)
  * Fixed address of FSF in debian/copyright
  * Sponsored by Petter Reinholtsen

 -- Michael Koch <konqueror@gmx.de>  Thu,  1 Sep 2005 08:09:49 +0000

varconf (0.6.2-2) unstable; urgency=low

  * Renamed packages due to C++ transition
  * Updated Standards-Version to 3.6.2
  * Sponsored by Petter Reinholtsen

 -- Michael Koch <konqueror@gmx.de>  Sat, 27 Aug 2005 10:22:46 +0000

varconf (0.6.2-1) unstable; urgency=low

  * New upstream release
  * Use --disable-dependency-tracking

 -- Michael Koch <konqueror@gmx.de>  Mon, 25 Oct 2004 12:30:41 +0000

varconf (0.6.1-3) unstable; urgency=high

  * Reworked package descriptions
  * Added debian/watch

 -- Michael Koch <konqueror@gmx.de>  Thu, 30 Sep 2004 06:55:51 +0000

varconf (0.6.1-2) unstable; urgency=low

  * debian/copyright: Fixed download url and copyright notice
  * debian/rules: Use dh_install instead of dh_movefiles
  * debian/control: Removed redundant Origin tag

 -- Michael Koch <konqueror@gmx.de>  Mon, 26 Jul 2004 09:02:43 +0200

varconf (0.6.1-1) unstable; urgency=low

  * New upstream release

 -- Michael Koch <konqueror@gmx.de>  Thu, 19 Feb 2004 14:03:51 +0100

varconf (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * Renamed deb files to match library version.
  * Bumped Standards-Version to 3.5.9.

 -- Michael Koch <konqueror@gmx.de>  Fri, 26 Apr 2003 10:43:24 +0100

varconf (0.5.4-5) unstable; urgency=low

  * Regenerated build files (Closes: #176789).

 -- Michael Koch <konqueror@gmx.de>  Fri, 21 Mar 2003 14:40:11 +0100

varconf (0.5.4-4) unstable; urgency=low

  * c102 transition.
  * Explicitely link to libstdc++.
  * Build-Depend on libsigc++-1.2-dev instead of libsigc++-dev.
  * debian/rules cleanup.

 -- Michael Koch <konqueror@gmx.de>  Fri, 10 Jan 2003 13:08:00 +0100

varconf (0.5.4-3) unstable; urgency=low

  * Build-Conflicts on libsigc++-1.1-dev and libsigc++-1.2-dev.
  * Bumped Standards-Version to 3.5.8.
  * Fixed descriptions in debian/control.
  * Support build option "noopt" in debian/rules.
  * Clean up debian/rules.
  * Added --disable-static to configure call in debian/rules.

 -- Michael Koch <konqueror@gmx.de>  Thu, 12 Dec 2002 18:27:42 +0100

varconf (0.5.4-2) unstable; urgency=low

  * Build-Conflicts to libsigc++-1.1-dev.

 -- Michael Koch <konqueror@gmx.de>  Mon, 23 Sep 2002 15:01:44 +0200

varconf (0.5.4-1) unstable; urgency=low

  * New upstream release
  * Dont Build-Depend on libstdc++-dev
  * Dont Depend on libstdc++-dev and libc6-dev
  * Updated Standard-Version to 3.5.7

 -- Michael Koch <konqueror@gmx.de>  Tue,  3 Sep 2002 07:03:32 +0200

varconf (0.5.3-4) unstable; urgency=low

  * Changed priority to extra.
  * Removed unneeded Build-Depends on libc6-dev.

 -- Michael Koch <konqueror@gmx.de>  Mon, 26 Aug 2002 21:57:14 +0200

varconf (0.5.3-3) unstable; urgency=low

  * fixed debian copyright
  * added documentation files

 -- Michael Koch <konqueror@gmx.de>  Sat,  3 Aug 2002 21:14:31 +0200

varconf (0.5.3-2) unstable; urgency=low

  * renamed all packages to include the (correct) SONAME
  * added package including libs with debug symbols

 -- Michael Koch <konqueror@gmx.de>  Wed, 31 Jul 2002 13:17:10 +0200

varconf (0.5.3-1) unstable; urgency=low

  * New upstream release
  * updated dependencies

 -- Michael Koch <konqueror@gmx.de>  Sat, 20 Jul 2002 19:56:49 +0200

varconf (0.5.2-5) unstable; urgency=low

  * fix shlibs version

 -- Michael Koch <konqueror@gmx.de>  Wed, 26 Jun 2002 09:13:08 +0200

varconf (0.5.2-4) unstable; urgency=low

  * added shlibs version

 -- Michael Koch <konqueror@gmx.de>  Fri, 21 Jun 2002 22:05:36 +0200

varconf (0.5.2-3) unstable; urgency=low

  * applied patch from rsteinke

 -- Michael Koch <konqueror@gmx.de>  Wed, 19 Jun 2002 22:03:18 +0200

varconf (0.5.2-2) unstable; urgency=low

  * fixed dependencies

 -- Michael Koch <konqueror@gmx.de>  Sun,  9 Jun 2002 11:50:57 +0200

varconf (0.5.2-1) unstable; urgency=low

  * New upstream release

 -- Michael Koch <konqueror@gmx.de>  Wed, 22 May 2002 15:05:29 +0200

varconf (0.5.0-1) unstable; urgency=low

  * Initial Debian version.

 -- Anders Petersson <demitar@innocent.com>  Sun, 18 Nov 2001 17:01:13 +0100
