Source: varconf
Section: libs
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Olek Wojnar <olek@debian.org>,
           Debian Games Team <team+pkg-games@tracker.debian.org>,
Build-Depends: debhelper-compat (= 13),
               libsigc++-2.0-dev,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://www.worldforge.org/
Vcs-Git: https://salsa.debian.org/games-team/varconf.git
Vcs-Browser: https://salsa.debian.org/games-team/varconf

Package: libvarconf-1.0-8v5
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends},
         ${shlibs:Depends}
Conflicts: libvarconf-1.0-8
Replaces: libvarconf-1.0-8
Description: WorldForge configuration file handling library
 Varconf is a configuration system designed for the STAGE server. Varconf
 can parse configuration files, command-line arguments and environment
 variables. It supports callbacks and can store its configuration
 information in separate Config objects or in one global configuration
 instance.
 .
 This package contains the shared libraries.

Package: libvarconf-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Pre-Depends: ${misc:Pre-Depends}
Depends: libsigc++-2.0-dev,
         libvarconf-1.0-8v5 (= ${binary:Version}),
         ${misc:Depends}
Replaces: libvarconf-1.0-dev
Conflicts: libvarconf-1.0-dev
Description: WorldForge configuration file handling library - development files
 Varconf is a configuration system designed for the STAGE server. Varconf
 can parse configuration files, command-line arguments and environment
 variables. It supports callbacks and can store its configuration
 information in separate Config objects or in one global configuration
 instance.
 .
 This package contains the development files for compiling applications and
 libraries depending on varconf.
